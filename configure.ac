AC_PREREQ(2.60)
AC_INIT([udev],
       [195],
       [],
       [udev],
       [https://bitbucket.org/braindamaged/udev])
AC_CONFIG_SRCDIR([src/udevd.c])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE([foreign 1.10 -Wall -Wno-portability silent-rules tar-pax no-dist-gzip dist-xz subdir-objects])
AC_USE_SYSTEM_EXTENSIONS
AC_SYS_LARGEFILE
AM_SILENT_RULES([yes])
LT_INIT([disable-static])
AC_PROG_AWK
AC_PROG_SED
AC_PROG_MKDIR_P

AC_PREFIX_DEFAULT([/usr])

AC_PATH_PROG([XSLTPROC], [xsltproc])
AM_CONDITIONAL(HAVE_XSLTPROC, test x"$XSLTPROC" != x)

AC_SEARCH_LIBS([clock_gettime], [rt], [], [AC_MSG_ERROR([POSIX RT library not found])])

AC_ARG_WITH([rootprefix],
       AS_HELP_STRING([--with-rootprefix=DIR], [rootfs directory prefix for config files and kernel modules]),
       [], [with_rootprefix=${ac_default_prefix}])
AC_SUBST([rootprefix], [$with_rootprefix])

AC_ARG_WITH([rootlibdir],
       AS_HELP_STRING([--with-rootlibdir=DIR], [rootfs directory to install shared libraries]),
       [], [with_rootlibdir=$libdir])
AC_SUBST([rootlib_execdir], [$with_rootlibdir])

AC_ARG_WITH([selinux],
       AS_HELP_STRING([--with-selinux], [enable SELinux support]),
       [], [with_selinux=no])
AS_IF([test "x$with_selinux" = "xyes"], [
       LIBS_save=$LIBS
       AC_CHECK_LIB(selinux, getprevcon,
              [],
              AC_MSG_ERROR([SELinux selected but libselinux not found]))
       LIBS=$LIBS_save
       SELINUX_LIBS="-lselinux -lsepol"
       AC_DEFINE(WITH_SELINUX, [1] ,[SELinux support.])
])
AC_SUBST([SELINUX_LIBS])
AM_CONDITIONAL(WITH_SELINUX, [test "x$with_selinux" = "xyes"])

AC_ARG_ENABLE([debug],
       AS_HELP_STRING([--enable-debug], [enable debug messages @<:@default=disabled@:>@]),
       [], [enable_debug=no])
AS_IF([test "x$enable_debug" = "xyes"], [ AC_DEFINE(ENABLE_DEBUG, [1], [Debug messages.]) ])

AC_ARG_ENABLE([logging],
       AS_HELP_STRING([--disable-logging], [disable system logging @<:@default=enabled@:>@]),
       [], enable_logging=yes)
AS_IF([test "x$enable_logging" = "xyes"], [ AC_DEFINE(ENABLE_LOGGING, [1], [System logging.]) ])

AC_ARG_ENABLE([manpages],
        AS_HELP_STRING([--disable-manpages], [disable man pages @<:@default=enabled@:>@]),
        [], enable_manpages=yes)
AS_IF([test "x$enable_manpages" = xyes -a "x$XSLTPROC" = x],
       [AC_MSG_ERROR([*** Manpages requested but xsltproc not found])])
AM_CONDITIONAL([ENABLE_MANPAGES], [test "x$enable_manpages" = "xyes"])

if test "x$cross_compiling" = "xno" ; then
       AC_CHECK_FILES([/usr/share/pci.ids], [pciids=/usr/share/pci.ids])
       AC_CHECK_FILES([/usr/share/hwdata/pci.ids], [pciids=/usr/share/hwdata/pci.ids])
       AC_CHECK_FILES([/usr/share/misc/pci.ids], [pciids=/usr/share/misc/pci.ids])
fi

AC_ARG_WITH(usb-ids-path,
       [AS_HELP_STRING([--with-usb-ids-path=DIR], [Path to usb.ids file])],
       [USB_DATABASE=${withval}],
       [if test -n "$usbids" ; then
              USB_DATABASE="$usbids"
       else
              PKG_CHECK_MODULES(USBUTILS, usbutils >= 0.82)
              AC_SUBST([USB_DATABASE], [$($PKG_CONFIG --variable=usbids usbutils)])
       fi])
AC_MSG_CHECKING([for USB database location])
AC_MSG_RESULT([$USB_DATABASE])
AC_SUBST(USB_DATABASE)

AC_ARG_WITH(pci-ids-path,
       [AS_HELP_STRING([--with-pci-ids-path=DIR], [Path to pci.ids file])],
       [PCI_DATABASE=${withval}],
       [if test -n "$pciids" ; then
              PCI_DATABASE="$pciids"
       else
              AC_MSG_ERROR([pci.ids not found, try --with-pci-ids-path=])
       fi])
AC_MSG_CHECKING([for PCI database location])
AC_MSG_RESULT([$PCI_DATABASE])
AC_SUBST(PCI_DATABASE)

AC_ARG_WITH(firmware-path,
       AS_HELP_STRING([--with-firmware-path=DIR[[[:DIR[...]]]]],
          [Firmware search path (default=ROOTPREFIX/lib/firmware/updates:ROOTPREFIX/lib/firmware)]),
       [], [with_firmware_path="$rootprefix/lib/firmware/updates:$rootprefix/lib/firmware"])
OLD_IFS=$IFS
IFS=:
for i in $with_firmware_path; do
       if test "x${FIRMWARE_PATH}" = "x"; then
              FIRMWARE_PATH="\\\"${i}/\\\""
       else
              FIRMWARE_PATH="${FIRMWARE_PATH}, \\\"${i}/\\\""
       fi
done
IFS=$OLD_IFS
AC_SUBST([FIRMWARE_PATH], [$FIRMWARE_PATH])


# ------------------------------------------------------------------------------
# kmod - library and tools for managing linux kernel modules
# ------------------------------------------------------------------------------

have_kmod=no
AC_ARG_ENABLE(kmod, AS_HELP_STRING([--disable-kmod], [disable loadable modules support]))
if test "x$enable_kmod" != "xno"; then
        PKG_CHECK_MODULES(KMOD, [ libkmod >= 5 ],
                [AC_DEFINE(HAVE_KMOD, 1, [Define if kmod is available]) have_kmod=yes], have_kmod=no)
        if test "x$have_kmod" = xno -a "x$enable_kmod" = xyes; then
                AC_MSG_ERROR([kmod support requested but libraries not found])
        fi
fi
AM_CONDITIONAL(HAVE_KMOD, [test "$have_kmod" = "yes"])

# ------------------------------------------------------------------------------
# blkid - block device's information
# ------------------------------------------------------------------------------

have_blkid=no
AC_ARG_ENABLE(blkid, AS_HELP_STRING([--disable-blkid], [disable blkid support]))
if test "x$enable_blkid" != "xno"; then
        PKG_CHECK_MODULES(BLKID, [ blkid >= 2.20 ],
                [AC_DEFINE(HAVE_BLKID, 1, [Define if blkid is available]) have_blkid=yes], have_blkid=no)
        if test "x$have_blkid" = xno -a "x$enable_blkid" = xyes; then
                AC_MSG_ERROR([blkid support requested but libraries not found])
        fi
fi
AM_CONDITIONAL(HAVE_BLKID, [test "$have_blkid" = "yes"])

# ------------------------------------------------------------------------------
# GUdev - libudev gobject interface
# ------------------------------------------------------------------------------
AC_ARG_ENABLE([gudev],
       AS_HELP_STRING([--enable-gudev], [enable Gobject libudev support @<:@default=disabled@:>@]),
       [], [enable_gudev=no])
AS_IF([test "x$enable_gudev" = "xyes"], [ PKG_CHECK_MODULES([GLIB], [glib-2.0 >= 2.22.0 gobject-2.0 >= 2.22.0]) ])

AC_ARG_ENABLE([introspection],
       AS_HELP_STRING([--enable-introspection], [enable GObject introspection @<:@default=disabled@:>@]),
       [], [enable_introspection=no])
AS_IF([test "x$enable_introspection" = "xyes"], [
       PKG_CHECK_MODULES([INTROSPECTION], [gobject-introspection-1.0 >= 0.6.2])
       AC_DEFINE([ENABLE_INTROSPECTION], [1], [enable GObject introspection support])
       AC_SUBST([G_IR_SCANNER], [$($PKG_CONFIG --variable=g_ir_scanner gobject-introspection-1.0)])
       AC_SUBST([G_IR_COMPILER], [$($PKG_CONFIG --variable=g_ir_compiler gobject-introspection-1.0)])
       AC_SUBST([G_IR_GENERATE], [$($PKG_CONFIG --variable=g_ir_generate gobject-introspection-1.0)])
       AC_SUBST([GIRDIR], [$($PKG_CONFIG --define-variable=datadir=${datadir} --variable=girdir gobject-introspection-1.0)])
       AC_SUBST([GIRTYPELIBDIR], [$($PKG_CONFIG --define-variable=libdir=${libdir} --variable=typelibdir gobject-introspection-1.0)])
])
AM_CONDITIONAL([ENABLE_INTROSPECTION], [test "x$enable_introspection" = "xyes"])
AM_CONDITIONAL([ENABLE_GUDEV], [test "x$enable_gudev" = "xyes"])

# ------------------------------------------------------------------------------
# keymap - map custom hardware's multimedia keys
# ------------------------------------------------------------------------------
AC_ARG_ENABLE([keymap],
       AS_HELP_STRING([--disable-keymap], [disable keymap fixup support @<:@default=enabled@:>@]),
       [], [enable_keymap=yes])
AS_IF([test "x$enable_keymap" = "xyes"], [
       AC_PATH_PROG([GPERF], [gperf])
       if test -z "$GPERF"; then
              AC_MSG_ERROR([gperf is needed])
       fi

       AC_CHECK_HEADER([linux/input.h], [:], AC_MSG_ERROR([kernel headers not found]))
       AC_SUBST([INCLUDE_PREFIX], [$(echo '#include <linux/input.h>' | eval $ac_cpp -E - | sed -n '/linux\/input.h/ {s:.*"\(.*\)/linux/input.h".*:\1:; p; q}')])
])
AM_CONDITIONAL([ENABLE_KEYMAP], [test "x$enable_keymap" = "xyes"])

# ------------------------------------------------------------------------------
# mtd_probe - autoloads FTL module for mtd devices
# ------------------------------------------------------------------------------
AC_ARG_ENABLE([mtd_probe],
       AS_HELP_STRING([--disable-mtd_probe], [disable MTD support @<:@default=enabled@:>@]),
       [], [enable_mtd_probe=yes])
AM_CONDITIONAL([ENABLE_MTD_PROBE], [test "x$enable_mtd_probe" = "xyes"])

# ------------------------------------------------------------------------------
# rule_generator - persistent network and optical device rule generator
# ------------------------------------------------------------------------------
AC_ARG_ENABLE([rule_generator],
       AS_HELP_STRING([--enable-rule_generator], [enable persistent network + cdrom links support @<:@default=disabled@:>@]),
       [], [enable_rule_generator=no])
AM_CONDITIONAL([ENABLE_RULE_GENERATOR], [test "x$enable_rule_generator" = "xyes"])

# ------------------------------------------------------------------------------
# create_floppy_devices - historical floppy kernel device nodes (/dev/fd0h1440, ...)
# ------------------------------------------------------------------------------
AC_ARG_ENABLE([floppy],
       AS_HELP_STRING([--enable-floppy], [enable legacy floppy support @<:@default=disabled@:>@]),
       [], [enable_floppy=no])
AM_CONDITIONAL([ENABLE_FLOPPY], [test "x$enable_floppy" = "xyes"])

my_CFLAGS="-Wall \
-Wmissing-declarations -Wmissing-prototypes \
-Wnested-externs -Wpointer-arith \
-Wpointer-arith -Wsign-compare -Wchar-subscripts \
-Wstrict-prototypes -Wshadow \
-Wformat-security -Wtype-limits"
AC_SUBST([my_CFLAGS])

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES([
        Makefile
        docs/Makefile
        docs/Doxyfile
])

# ------------------------------------------------------------------------------
# Documentation
# ------------------------------------------------------------------------------
DX_DOXYGEN_FEATURE(OFF)
DX_DOT_FEATURE(OFF)
DX_HTML_FEATURE(ON)
DX_CHM_FEATURE(OFF)
DX_CHI_FEATURE(OFF)
DX_MAN_FEATURE(OFF)
DX_RTF_FEATURE(OFF)
DX_XML_FEATURE(OFF)
DX_PDF_FEATURE(OFF)
DX_PS_FEATURE(OFF)

DX_INIT_DOXYGEN([udev], [], [API])

case $DX_FLAG_doc in
    1) AC_SUBST(enable_doxygen, yes) ;;
    0) AC_SUBST(enable_doxygen,  no) ;;
esac

# ------------------------------------------------------------------------------
# Report
# ------------------------------------------------------------------------------
AC_OUTPUT
AC_MSG_RESULT([
        $PACKAGE $VERSION
        ========

        prefix:                  ${prefix}
        rootprefix:              ${rootprefix}
        sysconfdir:              ${sysconfdir}
        bindir:                  ${bindir}
        libdir:                  ${libdir}
        rootlibdir:              ${rootlib_execdir}
        libexecdir:              ${libexecdir}
        datarootdir:             ${datarootdir}
        mandir:                  ${mandir}
        includedir:              ${includedir}
        include_prefix:          ${INCLUDE_PREFIX}
        firmware path:           ${FIRMWARE_PATH}
        usb.ids:                 ${USB_DATABASE}
        pci.ids:                 ${PCI_DATABASE}

        compiler:                ${CC}
        cflags:                  ${CFLAGS}
        ldflags:                 ${LDFLAGS}
        xsltproc:                ${XSLTPROC}
        gperf:                   ${GPERF}

        logging:                 ${enable_logging}
        debug:                   ${enable_debug}
        selinux:                 ${with_selinux}

        blkid:                   ${have_blkid}
        kmod:                    ${have_kmod}

        gudev:                   ${enable_gudev}
        gintrospection:          ${enable_introspection}
        keymap:                  ${enable_keymap}
        mtd_probe:               ${enable_mtd_probe}
        rule_generator:          ${enable_rule_generator}
        floppy:                  ${enable_floppy}

        documentation:           ${enable_doxygen}
        man pages:               ${enable_manpages}
])
